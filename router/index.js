// Importar el módulo body-parser y el framework Express
const bodyParser = require("body-parser");
const express = require("express");

// Crear una instancia de Router
const router = express.Router();

// Ruta principal que muestra una página con los datos del array "datos"
router.get("/", (req, res) => {
    res.render("index.html", {
      Header: "Sistemas Microinformatica",
      Footer: "Compañia de Luz @Derechos Reservados 2023 Paez Ayala",
    });
});

// Ruta que muestra una página con una tabla vacía que espera un parámetro llamado "numero" en la URL
router.get("/tabla", (req, res) => {
    const params = {
     numero: req.query.numero
    };
    res.render("tabla.html", params);
});

// Ruta que recibe el número enviado a través del formulario de la página "/tabla" y lo muestra en una tabla
router.post("/tabla", (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render("tabla.html", params);
});
router.get("/tabla", (req, res) => {
    const params = {
     numero: req.query.numero
    };
    res.render("tabla.html", params);
});

// Ruta que recibe el número enviado a través del formulario de la página "/tabla" y lo muestra en una tabla
router.post("/ingreso", (req, res) => {
    const params = {
        NombreFinal : req.body.NombreFinal,
        GradoFinal : req.body.GradoFinal,
        DiasFinal: req.body.DiasFinal,
        PagoFinal: req.body.PagoFinal,
        SubTotal: req.body.SubTotal,
        Impuesto: req.body.Impuesto,
        TotalAPagar: req.body.TotalAPagar


    };
    res.render("ingreso.html", params);
});
router.get("/ingreso", (req, res) => {
    const params = {
      NombreFinal : req.query.NombreFinal,
     GradoFinal : req.query.GradoFinal,
     DiasFinal: req.query.DiasFinal,
     PagoFinal: req.query.PagoFinal,
     SubTotal: req.query.SubTotal,
     Impuesto: req.query.Impuesto,
     TotalAPagar: req.query.TotalAPagar
    };
    res.render("ingreso.html", params);
});
// Exportar el módulo Router para que pueda ser utilizado en otros archivos
module.exports = router;